// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var $ = require('jquery');
var app = express();

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	html='';
	next();
});

// Serve static files
app.use(express.static('public'));

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port);
});
